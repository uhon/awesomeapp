# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.

role :app, 'uhon@awesomeserver'
#role :web, %w{deploy@example.com}
#role :db,  %w{deploy@example.com}


# The server's user for deploys
set :user, 'uhon'
set :use_sudo, false

# Name of your applicaiton (as on filesystem)
set :application, 'awesomeapp'
# Remote Repository
set :repo_url, 'git@bitbucket.org:uhon/awesomeapp.git'


set :keep_releases, 10

# SCM
set :scm, :git
set :branch, 'master'
set :scm_verbose, true

set :deploy_to, "/var/www/#{fetch(:application)}"

# Shared Paths
set :shared_path, "#{fetch(:deploy_to)}/shared"





