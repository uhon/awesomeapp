# config valid only for Capistrano 3.1
lock '3.2.1'

namespace :deploy do
  desc 'symlink uploaded folder into current application)'
  task :linkUploads do
    on roles(:app) do
      execute "ln -sf #{fetch(:shared_path)}/uploaded #{release_path}/uploaded"
    end
  end

  before :publishing, :linkUploads
end


desc 'logtails /var/log/syslog or any other logfile given as parameter'
task :log, :arg1 do |t, args|
  on roles(:app) do
    logfile = args[:arg1]
    if logfile == nil
      logfile = '/var/log/syslog'
    end

    trap('INT') { puts 'Interupted'; exit 0; }
    execute "tail -f #{logfile}"
  end
end


